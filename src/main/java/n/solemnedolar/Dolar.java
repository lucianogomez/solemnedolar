package n.solemnedolar;

import java.io.StringReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;



@Path("/")
public class Dolar {
    @GET
    @Path("/fecha")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public String getFehcaQuery(@QueryParam("dia") Integer dia, @QueryParam("mes") Integer mes, @QueryParam("anio") Integer anio ){
        
        String respuesta;
        String url;
        Response resp;
        boolean respu = true;
                
        String apiKey = "8bad67e1bf69c7a6888eb420c3d23605b46f1dc1";
        
        Client client = ClientBuilder.newClient();
        
        do{
            url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/"+
                anio+"/"+mes+"/dias/"+dia+"/?formato=JSON&apikey="+apiKey;
            resp = client.target(url).request().get();
            if(resp.getStatus() == 200) respu = false;
            dia--;
        }while(respu);{
            respuesta = resp.readEntity(String.class);            
        }
        
        JsonReader j = Json.createReader(new StringReader(respuesta));
        JsonObject jobj = j.readObject();
        
        JsonArray subJsonArray = jobj.getJsonArray("Dolares");
        JsonObject jsonExtraido = subJsonArray.getJsonObject(0);
        
        String valorDelDolar = jsonExtraido.getString("Valor");
        String fechaDelDolar = jsonExtraido.getString("Fecha");
     
        JsonObject jr = Json.createObjectBuilder()
                .add("Fuente", "SBIF")
                .add("Valor Dolar", jsonExtraido.getString("Valor"))
                .add("Fecha", jsonExtraido.getString("Fecha"))
                .build();
        
        
        return jr.toString();
    }    
}


