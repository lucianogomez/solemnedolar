<%-- 
    Document   : index
    Created on : 26-09-2019, 17:14:26
    Author     : Usuario
--%>

<%@page import="javax.ws.rs.core.Response"%>
<%@page import="java.util.Calendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    
    <% 
        Calendar fechaHoy = Calendar.getInstance();
        int hoy = fechaHoy.get(Calendar.DAY_OF_MONTH);
        int mes = fechaHoy.get(Calendar.MONTH)+1;
        int anio = fechaHoy.get(Calendar.YEAR);
        
        
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body style="width: 300px; margin: 10px auto;">
        
                
        <div class="card bg-light mb-3" style="max-width: 18rem;">
          <div class="card-header">Consultar dolar</div>
          <div class="card-body">
            <form action="api/fecha?" method=”get”>
                <div class="form-group">
                    <label for="dia">Dia</label>
                    <input type="number" name="dia" value="<%= hoy%>" max="31" min="1">
                </div>
                <div class="form-group">
                    <label for="mes">Mes</label>
                    <input type="number" name="mes" value="<%= mes%>" max="12" min="1">
                </div>
                <div class="form-group">
                    <label for="anio">Año</label>
                    <input type="number" name="anio" value="<%= anio%>" max="<%= anio%>">
                </div>
                <input type="submit" value="Consultar">
            </form>
                <c:if test="${not empty student}">
                    <h1>${student}</h1>
                </c:if>
          </div>
        </div>
        <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
            <div class="card-header">Como Funciona la Api</div>
            <div class="card-body">
              <p class="card-text">Nuestra api consulta la api de que entrga SBIF la cual nostros le enviamos una fecha habil para una respues ok. </p>
            </div>
          </div>        
    </body>
</html>
